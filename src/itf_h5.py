import thutils
import numpy as np
import sklearn.preprocessing
import tables as tbl

datah5 = tbl.open_file('/home/data/thumos/2014/data.h5')

traindata = thutils.read_features(datah5, ['/thumos/features/itf/train/bof/tr', '/thumos/features/itf/train/bof/hog', '/thumos/features/itf/train/bof/hof', '/thumos/features/itf/train/bof/mbh'], norm='l2')
traindata = np.array(traindata, dtype=np.float32)
trainlabels = datah5.root.thumos.metadata.train.col('actions_indices')

valdata = thutils.read_features(datah5, ['/thumos/features/itf/validation/bof/tr', '/thumos/features/itf/validation/bof/hog', '/thumos/features/itf/validation/bof/hof', '/thumos/features/itf/validation/bof/mbh'], norm='l2')
valdata = np.array(valdata, dtype=np.float32)
vallabels = datah5.root.thumos.metadata.validation.col('actions_indices')

testdata = thutils.read_features(datah5, ['/thumos/features/itf/test/bof/tr', '/thumos/features/itf/test/bof/hog', '/thumos/features/itf/test/bof/hof', '/thumos/features/itf/test/bof/mbh'], norm='l2')
testdata = np.array(testdata, dtype=np.float32)
testlabels = datah5.root.thumos.metadata.test.col('actions_indices')

dataFile = tbl.open_file('/home/data/thumos/2014/itf_l2.h5', 'w', filters=tbl.Filters(complevel=9))

dataFile.create_array('/train', 'X', np.hstack([traindata, trainlabels]), createparents=True)
dataFile.create_array('/validation', 'X', np.hstack([valdata, vallabels]), createparents=True)
dataFile.create_array('/test', 'X', np.hstack([testdata, testlabels]), createparents=True)
dataFile.create_array('/trainval', 'X', np.vstack([dataFile.root.train.X, dataFile.root.validation.X]), createparents=True)
dataFile.close()
