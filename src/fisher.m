vlFeatPath = '/home/fmpaezri/utils/vlfeat-0.9.18'
h5file = '/home/data/thumos/2014/itf.h5'
h5inputDataset = '/thumos/features/itf/mbh'
h5outputDataset = '/thumos'

run(strcat(vlFeatPath, '/toolbox/vl_setup'))
vl_version verbose

numClusters = 256
data = h5read(h5file, )
[means, covariances, priors] = vl_gmm(data, numClusters);
encoding = vl_fisher(datatoBeEncoded, means, covariances, priors, 'Improved', 'Verbose', 'Fast');
