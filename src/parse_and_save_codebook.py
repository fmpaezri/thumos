import logging



import tables as tbl
import numpy as np
import os
import argparse as ap

def read_words(path):
    '''Parses plain text, space separated, row vectors corresponding to visual words'''
    lines = open(path).readlines()
    words = None
    index = 0
    for line in lines:
        tokens = line.strip().split()
        values = map(float, tokens)
        if words == None:
            words = np.empty((len(lines), len(values)))
        #TODO: validate constant word dimension
        words[index] = values
        index += 1
    return words

def parse_save_codebooks(codebookDir, h5Path, codebooksGroup, codebookExtension = '.txt'):
    '''Parses all plain files with the given extension under the specified folder and saves them to the specified group'''
    h5File = tbl.open_file(h5Path, 'a')
    #TODO: handle case where path was specified including separator at the end
    groupTokens = codebooksGroup.split('/')
    parentPath = '/'.join(groupTokens[:-1])
    leafPath = groupTokens[-1]
    group = h5File.create_group(parentPath, leafPath, createparents=True)
    for featureDict in os.listdir(codebookDir):
        filePath = os.path.join(codebookDir, featureDict)
        if not featureDict.endswith(codebookExtension):
            continue
        words = read_words(filePath)
        #create node and save
        #TODO: handle ext not present or not at end
        featureName = featureDict[:featureDict.index(codebookExtension)].lower()
        h5File.create_array(group, featureName, words)
    h5File.close()

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Parses plain text files under a folder to save the stored codebooks in an H5 file')
    parser.add_argument('codebookDir', help='Folder where the plain text files with the words are stored')
    parser.add_argument('h5Path', help='Path to H5 file where parsed data will be saved')
    parser.add_argument('codebooksGroup', help='Group path where the codebooks are going to be stored')
    parser.add_argument('--codebookExtension', help='Extension used to filter files on the folder and parse feature names', default='.txt')

    args = parser.parse_args()
    parse_save_codebooks(args.codebookDir, args.h5Path, args.codebooksGroup)
