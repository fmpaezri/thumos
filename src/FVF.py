import numpy
import tables as tbl

HOG_LEN = 96
HOF_LEN = 108
MBHX_LEN = 96
MBHY_LEN = 96

class FVF(tbl.IsDescription):
    xnorm = tbl.Float16Col(pos=0)
    ynorm = tbl.Float16Col(pos=1) 
    tnorm = tbl.Float16Col(pos=2)
    pts = tbl.UInt32Col(pos=3)
    StartPTS = tbl.UInt32Col(pos=4)
    EndPTS = tbl.UInt32Col(pos=5)
    Xoffset = tbl.UInt16Col(pos=6)
    Yoffset = tbl.UInt16Col(pos=7)
    PatchWidth = tbl.UInt8Col(pos=8)
    PatchHeight = tbl.UInt8Col(pos=9)
    hog = tbl.Float16Col(shape=(HOG_LEN,), pos=10)
    hof = tbl.Float16Col(shape=(HOF_LEN,), pos=11)
    mbhx = tbl.Float16Col(shape=(MBHX_LEN,), pos=12)
    mbhy = tbl.Float16Col(shape=(MBHY_LEN,), pos=14)

DTYPE = [('xnorm', numpy.float16), ('ynorm', numpy.float16), ('tnorm', numpy.float16), ('pts', numpy.uint32), ('StartPTS', numpy.uint32), ('EndPTS', numpy.uint32), ('Xoffset', numpy.uint16), ('Yoffset', numpy.uint16), ('PatchWidth', numpy.uint8), ('PatchHeight', numpy.uint8), ('hog', numpy.float16, (HOG_LEN,)), ('hof', numpy.float16, (HOF_LEN,)), ('mbhx', numpy.float16, (MBHX_LEN,)), ('mbhy', numpy.float16, (MBHY_LEN,))]

def load(filePath):
    return numpy.loadtxt(filePath, dtype=DTYPE)

def parse(line):
    tokens = line.strip().split()
    index = 0
    xnorm = float(tokens[index])
    index += 1
    ynorm = float(tokens[index])
    index += 1
    tnorm = float(tokens[index])
    index += 1
    pts = int(tokens[index])
    index += 1
    StartPTS = int(tokens[index])
    index += 1
    EndPTS = int(tokens[index])
    index += 1
    Xoffset = int(tokens[index])
    index += 1
    Yoffset = int(tokens[index])
    index += 1
    PatchWidth = int(tokens[index])
    index += 1
    PatchHeight = int(tokens[index])
    index += 1
    hog = map(float, tokens[index:index+HOG_LEN])
    index += HOG_LEN
    hof = map(float, tokens[index:index+HOF_LEN])
    index += HOF_LEN
    mbhx = map(float, tokens[index:index+MBHX_LEN])
    index += MBHX_LEN
    mbhy = map(float, tokens[index:index+MBHY_LEN])
    index += MBHY_LEN
    if not index == len(tokens):
        raise Exception('Descriptor line contains more tokens than expected')
    return [xnorm, ynorm, tnorm, pts, StartPTS, EndPTS, Xoffset, Yoffset, PatchWidth, PatchHeight, hog, hof, mbhx, mbhy]
