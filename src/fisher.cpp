#include <vl/fisher.h>

#include <ctime>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>

void usage(char* name){
    std::cout<<"Usage: "<<name<<" weights means covs"<<std::endl;
}

void read_matrix(std::istream &input, std::vector< std::vector<float> >& data, int debug = 0){
    std::string line;
    while(std::getline(input, line) && line.size() > 0){
        if(debug != 0){
            std::cout<<line<<std::endl;
        }
        std::vector<float> row;
        std::istringstream inputStream(line);
        std::copy(std::istream_iterator<float>(inputStream),
                std::istream_iterator<float>(),
                std::back_inserter(row));
        data.push_back(row);
    }
}

float* vectors2array(std::vector< std::vector<float> >& data){
    float* copy = new float[data.size()*data[0].size()];
    //std::cerr<<"Allocated memory at "<<copy<<" for vectors2array"<<std::endl;
    for(int i = 0; i < data.size(); i++){
        for(int j = 0; j < data[i].size(); j++){
            copy[i*data[i].size()+j] = data[i][j];
        }
    }
    return copy;
}

long encode(float* input, float* means, float* covars, float* weights, float* output, int dimension, int numClusters, int numDataToEncode){
    return vl_fisher_encode(
        output, VL_TYPE_FLOAT, means, dimension, numClusters, covars, weights, input, numDataToEncode, VL_FISHER_FLAG_IMPROVED
    );
}

int copy(float* input, float* output, int dimension){
    for(int i = 0; i < dimension; i++){
        output[i] = input[i];
    }
    return 0;
}

int main(int argc, char** argv){
    if(argc <= 3){
        std::cerr<<"Invalid number of arguments"<<std::endl;
        usage(argv[0]);
        exit(1);
    }

    //std::cerr<<"Starting program "<<argv[0]<<" with args "<<argv[1]<<","<<argv[2]<<","<<argv[2]<<","<<argv[3]<<std::endl;

    std::ifstream weightsStream(argv[1]);
    std::ifstream meansStream(argv[2]);
    std::ifstream covarsStream(argv[3]);

    std::vector< std::vector<float> > input;
    std::vector< std::vector<float> > weights;
    std::vector< std::vector<float> > means;
    std::vector< std::vector<float> > covars;

    read_matrix(std::cin, input);
    if(input.size() == 0){
        std::cerr<<"Empty input"<<std::endl;
        return 0;
    }

    //std::cerr<<"Read input"<<std::endl;
    /*for(int i = 0; i < input.size(); i++){
        for(int j = 0; j < input[i].size(); j++){
            std::cerr<<input[i][j]<<" ";
        }
        std::cerr<<std::endl;
    }*/

    read_matrix(weightsStream, weights);
    read_matrix(meansStream, means);
    read_matrix(covarsStream, covars);

    //std::cerr<<"Read GMM parameters"<<std::endl;

    int dimension = means[0].size();
    int numClusters = means.size();
    int numDataToEncode = input.size();
    float* aMeans = vectors2array(means);
    float* aCovars = vectors2array(covars);
    float* aWeights = vectors2array(weights);
    float* aInput = vectors2array(input);

    float* enc = (float*) vl_malloc(sizeof(float) * 2 * dimension * numClusters);

    std::time_t now = time(0);

    std::cerr<<std::ctime(&now)<<"Starting Fisher Encoding with dimension "<<dimension<<" numClusters "<<numClusters<<" numDataToEncode "<<numDataToEncode<<std::endl;

    int terms = vl_fisher_encode(
        enc, VL_TYPE_FLOAT, aMeans, dimension, numClusters, aCovars, aWeights, aInput, numDataToEncode, VL_FISHER_FLAG_IMPROVED
    );

    now = time(0);

    std::cerr<<std::ctime(&now)<<"Finished with terms "<<terms<<std::endl;

    for(int i = 0; i < 2 * dimension * numClusters; i++){
        std::cout<<enc[i];
        if(i != 2 * dimension * numClusters - 1){
            std::cout<<" ";
        }
    }
    std::cout<<std::endl;

    delete aMeans;
    delete aCovars;
    delete aWeights;
    delete aInput;
    vl_free(enc);

    return 0;
}
