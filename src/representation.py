import logging
import FVF

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

commandName = 'DenseTrackStab'

def launch_extraction(videopath):
    import subprocess
    process = subprocess.Popen([commandName, videopath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return process

def fisher(inputs, weights, means, covars, vlfeatPath='libvl.so'):
    import ctypes
    vlfeat = ctypes.cdll.LoadLibrary(vlfeatPath)
    sizeofFloat = 4
    vl_type_float = 1
    vl_fisher_flag_improved = 3
    numClusters, dimension = means.shape
    numDataToEncode = inputs.shape[0]
    vlfeat.vl_malloc.restype = ctypes.c_void_p
    vlfeat.vl_malloc.agrtypes = [ctypes.c_int]
    enc = vlfeat.vl_malloc(2*sizeofFloat*dimension*numClusters)
    #TODO: review flat ordering
    meansList = means.ravel().tolist()
    meansArray = (ctypes.c_float*len(meansList))()
    for i in range(len(meansList)):
        meansArray[i] = meansList[i]
    covarsList = covars.ravel().tolist()
    covarsArray = (ctypes.c_float*len(covarsList))()
    for i in range(len(covarsList)):
        covarsArray[i] = covarsList[i]
    weightsList = weights.ravel().tolist()
    weightsArray = (ctypes.c_float*len(weightsList))()
    for i in range(len(weightsList)):
        weightsArray[i] = weightsList[i]
    inputsList = inputs.ravel().tolist()
    inputsArray = (ctypes.c_float*len(inputsList))()
    for i in range(len(inputsList)):
        inputsArray[i] = inputsList[i]
    vlfeat.vl_fisher_encode.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_void_p, ctypes.c_int, ctypes.c_int, ctypes.c_void_p, ctypes.c_void_p, ctypes.c_void_p, ctypes.c_int, ctypes.c_int]
    terms = vlfeat.vl_fisher_encode(enc, vl_type_float, meansArray, dimension, numClusters, covarsArray, weightsArray, inputsArray, numDataToEncode, vl_fisher_flag_improved)
    encPointer = ctypes.cast(enc, ctypes.POINTER(ctypes.c_float))
    encoding = [encPointer[i] for i in xrange(2*dimension*numClusters)]
    vlfeat.vl_free.argtypes = [ctypes.c_void_p]
    vlfeat.vl_free(enc)
    #TODO: reshape
    return encoding

def process_features(videopath, outputFolder, extension, desc=None, parse=None, online=True):
    '''
    Extracts features and saves output to a single table HDF5 file
    '''
    import subprocess
    import os.path
    import tables
    import ITF
    if desc is None:
        desc = ITF.IDT
    if parse is None:
        parse = ITF.parse
    basename = os.path.basename(videopath).replace(extension, '')
    outputPath = os.path.join(outputFolder, basename + '.h5')
    if not os.path.exists(outputFolder):
        print 'Output folder {} doesnt exist, aborting'.format(outputFolder)
        return
    if os.path.exists(outputPath):
        print 'File {} already exists, aborting'.format(outputPath)
        return
    h5file = None
    try:
        if online:
            extraction_process = launch_extraction(videopath)
            lines = extraction_process.stdout
        else:
            output = subprocess.check_output([commandName, videopath])
            lines = output.splitlines()
        h5file = tables.open_file(outputPath, 'w', filters=tables.Filters(complevel=9))
        table = h5file.create_table(h5file.root, basename, description=desc)
        for line in lines:
            row = parse(line)
            table.append([row])
        table.flush()
        if online:
            extraction_process.wait()
    finally:
        if h5file is not None and h5file.isopen:
            h5file.close()

import os
import tables
import time
import numpy

#0.004 on default ITF params is ~1M
def sample(featuresPath, prop=0.004, suffix='.h5'):
    '''
    Generates a sample of the given percent of each HDF5 file based on features folder list filtered by suffix.
    Single node with filename-without-suffix as name is assumed
    '''
    import os, tables, numpy
    if not featuresPath.endswith(suffix):
        raise Exception('Path {} ends without required suffix {}'.format(featuresPath, suffix))
    h5file = tables.open_file(featuresPath, 'r')
    node = h5file.get_node('/'+os.path.basename(featuresPath).replace(suffix, ''))
    #generate a sample of ids with the given proportion
    ids = numpy.arange(node.nrows)
    numpy.random.shuffle(ids)
    ids = ids[:int(node.nrows*prop)]
    aSample = node[ids]
    h5file.close()
    return aSample

import sklearn.decomposition

FEATURE_NAMES = ['hog', 'hof', 'mbhx', 'mbhy']

def generate_pca(aSample, dump=False, suffix='_pca.pkl', featureNames=FEATURE_NAMES):
    '''
    Generates PCA for all features with half of original dimension
    '''
    pcaDict = {}
    for featureName in featureNames:
        startTime = time.time()
        pca = sklearn.decomposition.PCA(n_components=int(aSample[featureName].shape[1]/2), whiten=True)
        pca.fit(aSample[featureName])
        pcaDict[featureName] = pca
        elapsedTime = time.time() - startTime
        print 'Elapsed time: {} Generated PCA for feature: {}'.format(elapsedTime, featureName)
        if dump:
            pickle.dump(pcaDict[featureName], open(featureName+suffix, 'w'))
    return pcaDict

import pickle

def load_pca_dict(dataFolder, featureNames = FEATURE_NAMES):
    pcaFile = None
    pcaDict = {}
    for featureName in featureNames:
        try:
            pcaFile = open(os.path.join(dataFolder, 'pca', featureName + '_pca.pkl'), 'r')
            pca = pickle.load(pcaFile)
        finally:
            if pcaFile is not None and not pcaFile.closed:
                pcaFile.close()
        pcaDict[featureName] = pca
    return pcaDict

def generate_gmm(aSample, pca, n_components=256):
    '''
    Generates GMM for a single feature
    '''
    import sklearn.mixture, pickle, time
    startTime = time.time()
    pcaFeatures = pca.transform(aSample)
    pcaTime = time.time()
    print 'PCA Transform Time: {}'.format(pcaTime-startTime)
    model = sklearn.mixture.GMM(n_components=n_components)
    model.fit(pcaFeatures)
    gmmTime = time.time()
    print 'GMM Fitting Time: {}'.format(gmmTime-pcaTime)
    return model

def gmm_params_2_plain(rootFolder, gmmDict, featureNames = FEATURE_NAMES):
    '''
    Dumps on plain text files the params of the GMM on a predefined folder structure
    '''
    for featureName in featureNames:
        outputPath = os.path.join(rootFolder,'plain',featureName)
        os.mkdir(outputPath)
        gmm = gmmDict[featureName]
        numpy.savetxt(os.path.join(outputPath, 'means.txt'), gmm.means_)
        numpy.savetxt(os.path.join(outputPath, 'covars.txt'), gmm.covars_)
        numpy.savetxt(os.path.join(outputPath, 'weights.txt'), gmm.weights_)

def online_fisher(inputTuple, dataFolder, featureNames = FEATURE_NAMES, clusters=256):
    import subprocess
    import numpy
    import os
    import ITF
    import sys
    #import cv
    #import cv2
    import time
    videopath, pcaDict = inputTuple
    #video = cv2.VideoCapture(videopath)
    #frames = 1
    #if video.isOpened():
    #    frames = video.get(cv.CV_CAP_PROP_FRAME_COUNT)
    #    print 'Video {} has {} frames'.format(videopath, frames)
    #video.release()
    dtype = []
    for featureName in featureNames:
        dtype.append((featureName, numpy.float32, (2*pcaDict[featureName].n_components*clusters, )))
    fisherVectors = numpy.zeros((1,), dtype=dtype)
    initTime = time.time()
    extraction_process = launch_extraction(videopath)
    fisher_process = {}
    for featureName in featureNames:
        gmmItemsPath = [os.path.join(dataFolder, 'gmm', 'plain', featureName, item) for item in ['weights.txt', 'means.txt', 'covars.txt']]
        #used sys.stderr instead of subprocess.PIPE as function blocked/deadlocked when reading stderr
        process = subprocess.Popen(['fisher'] + gmmItemsPath, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        fisher_process[featureName] = process
    print 'Launched processes'
    lines = 0
    currentFrame = 0
    #print 'Percent {}%\r'.format(1.0*currentFrame/frames),
    for line in extraction_process.stdout:
        lines += 1
        row = ITF.parse(line)
        #if lines % 100 == 0:
        #    currentFrame = row[0]
        #    currentTime = time.time()
        #    print 'Percent {:04.2}% Frame {:07} Elapsed {:06} Lines {:09}\r'.format(1.0*currentFrame/frames, currentFrame, currentTime-initTime, lines),
        for featureName in featureNames:
            #2-d array so str will add arbitrary newlines, convert to 1-d by taking single row
            feature_row = pcaDict[featureName].transform(row[-len(featureNames)+featureNames.index(featureName)])[0]
            fisher_process[featureName].stdin.write(' '.join(map(str, feature_row)) + '\n' )
    print 'End of input lines {}'.format(lines)
    extraction_process.wait()
    for featureName in featureNames:
        fisher_process[featureName].stdin.close()
        print 'Current stderr for {}'.format(featureName)
        #print fisher_process[featureName].stderr.readlines()
        fisherPlain = fisher_process[featureName].stdout.readlines()
        if len(fisherPlain) == 1:
            fisherVectors[featureName] = map(numpy.float32, fisherPlain[0].split())
        else:
            print 'Found {} lines of encoding output'.format(len(fisherPlain)), fisherPlain
        #print fisher_process[featureName].stderr.readlines()
        fisher_process[featureName].wait()
    return (videopath, fisherVectors)

def fisher_encode(inputTuple, dataFolder, featureNames = FEATURE_NAMES, clusters=256):
    '''
    Generates an Improved Fisher Vector representation for the features in an HDF5 file.

    The inputs are the path to the HDF5 file with a single table holding all the data generated by the Improved Trajectory Features, and a dict with the sklearn.decomposition.PCA model. Optional parameters are: the base path to the directory structure where the GMM parameters in plain text format are located for each descriptor, name of features to be processed and number of clusters of the GMM.
    '''
    import tables
    import subprocess
    import numpy
    import pickle
    import os
    featuresFile = None
    #independet variables for each of the inputs
    featurePath, pcaDict = inputTuple
    #dtype is built using numpy's structured arrays list notation, shape is given by Fisher vector
    dtype = []
    for featureName in featureNames:
        dtype.append((featureName, numpy.float32, (2*pcaDict[featureName].n_components*clusters, )))
    try:
        featuresFile = tables.open_file(featurePath, 'r')
        filename = os.path.basename(featurePath).replace('.h5', '')
        featuresTable = featuresFile.get_node('/' + filename)
        #the single Fisher vector output for all descriptors
        fisherVectors = numpy.zeros((1,), dtype=dtype)
        for featureName in featureNames:
            #instead of being a parameter, PCA models could be read from files but it increases processing time
            pca = pcaDict[featureName]
            #reduced dimensional features
            pcaFeature = pca.transform(featuresTable.col(featureName))
            #predefined locations of GMM params for each feature, required by C++ executable
            gmmItemsPath = [os.path.join(dataFolder, 'gmm', 'plain', featureName, item) for item in ['weights.txt', 'means.txt', 'covars.txt']]
            #create process to call executable and pass GMM params paths
            process = subprocess.Popen(['fisher'] + gmmItemsPath, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            #write each feature line to process stdin
            for row in pcaFeature:
                process.stdin.write(' '.join(map(str, row)) + '\n' )
            #end stdin so executable may stop reading and starts processing
            process.stdin.close()
            fisherPlain = process.stdout.readlines()
            if len(fisherPlain) == 1:
                fisherVectors[featureName] = map(numpy.float32, fisherPlain[0].split())
            else:
                print 'Found {} lines of encoding output'.format(len(fisherPlain))
            print process.stderr.readlines()
    finally:
        if featuresFile is not None and featuresFile.isopen:
            featuresFile.close()
    #return the path so results may be mapped back to input file
    return (featurePath, fisherVectors)

def fisher_vector(X, gmm):
    '''
    Implementation of Fisher Vector following 
    Image Classification with the Fisher Vector: Theory and Practice, Sanchez et al., IJCV 2013

    X row stacking of features with D dimensions
    gmm sklearn's GMM model with K components
    returns 2*K*D Fisher vector
    '''
    import numpy
    import sklearn.mixture
    import time
    if not isinstance(gmm, sklearn.mixture.GMM):
        raise Exception('gmm parameter must be of type {}', sklearn.mixture.GMM)
    k = gmm.n_components
    d = X.shape[1]
    T = X.shape[0]
    w = gmm.weights_
    m = gmm.means_
    s = gmm.covars_
    print time.clock()
    gammaX = gmm.score_samples(X)[1]
    print time.clock()
    S0 = numpy.sum(gammaX,axis=0)
    #S0 = numpy.zeros((k,))
    S1 = numpy.empty((d,k))
    #S1 = numpy.zeros((d,k))
    S2 = numpy.empty((d,k))
    #S2 = numpy.zeros((d,k))
    for i in xrange(k):
        S1[:,i] = numpy.dot(X.T,gammaX[:,i])
        S2[:,i] = numpy.dot(X.T**2,gammaX[:,i])
    print time.clock()
    #for t in numpy.arange(T):
    #    for i in numpy.arange(k):
    #        S0[i] += gammaX[t,i]
    #        S1[:,i] += gammaX[t,i]*X[t,:]
    #        S2[:,i] += gammaX[t,i]*X[t,:]**2
    #Galpha = numpy.zeros((k,))
    #Gmean = numpy.zeros((d,k))
    #Gsigma = numpy.zeros((d,k))
    #for i in numpy.arange(k):
    #    Galpha[i] = (S0[i]-T*w[i])/(numpy.sqrt(w[i]))
    #    Gmean[:,i] = (S1[:,i]-m[i,:]*S0[i])/(numpy.sqrt(w[i])*s[i,:])
    #    Gsigma[:,i] = (S2[:,i]-2*m[i,:]*S1[:,i]+(m[i,:]**2-s[i,:]**2)*S0[i])/(numpy.sqrt(2*w[i])*s[i,:]**2)
    Gmean = (S1-m.T*S0)/(s.T*numpy.sqrt(w))
    Gsigma = (S2-2*m.T*S1+(m.T**2-s.T**2)*S0)/(s.T**2*numpy.sqrt(2*w))
    print time.clock()
    Glambda = numpy.concatenate([Gmean.T.ravel(), Gsigma.T.ravel()])
    Glambda = numpy.sign(Glambda)*numpy.sqrt(numpy.abs(Glambda))
    Glambda = Glambda/numpy.sqrt(numpy.sum(Glambda**2))
    return Glambda

def gaussian_pdf(xt, M, S):
    import theano
    import theano.tensor as T
    import numpy
    delta = M-xt
    e = -1/2*(delta*1/S*delta).sum(axis=0)
    return T.exp(e)/((2*numpy.pi)**(xt.shape[0])/2)*T.sqrt((S**2).sum())

def gaussian_posterior(xt, W, M, S):
    pdf = gaussian_pdf(xt, M, S)
    resp = W*pdf
    return resp/resp.sum(axis=0)

import argparse as ap
import tables
import ITF

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Generates Fisher vector representation for videos under a given folder')
    parser.add_argument('folder', help='Folder containing videos')
    parser.add_argument('output', help='Path of output HDF5 file')
    parser.add_argument('node', help='Name of node to store vectors')
    args = parser.parse_args()

    filenames = os.listdir(args.folder)
    filenames = sorted(filenames)
    logging.debug('Found {} files on folder {}'.format(len(filenames), args.folder))
    pcaDict = load_pca_dict()
    h5file = tables.open_file(args.output, 'w', filters=tables.Filters(complevel=9))
    group = os.path.dirname(args.node)
    node = os.path.basename(args.node)
    table = None
    for file in filenames:
        fv = online_fisher(os.path.join(args.folder,file), pcaDict)
        if table is None:
            table = h5file.create_table(group, node, description=fv.dtype, createparents=True)
        table.append(fv)
    table.flush()
    h5file.close()
