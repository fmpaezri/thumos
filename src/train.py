import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

import tables as tbl
import sklearn
import sklearn.cross_validation
import sklearn.svm
import sklearn.metrics
import sklearn.pipeline
import sklearn.preprocessing
import sklearn.grid_search
import numpy as np
import thutils

def classifier():
    clf = sklearn.svm.SVC(kernel='linear')
    return clf

import argparse as ap

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Trains a model on a development set of train data and tests it on remaining eval set')
    parser.add_argument('input', help='File with dataset information')
    parser.add_argument('metadata', help='Node with metadata')
    parser.add_argument('index', help='Class index path')
    parser.add_argument('bofNodes', help='List of nodes with BOF to concatenate', nargs='+')
    parser.add_argument('--norm', help='Norm applied to each descriptor', default=None)
    parser.add_argument('--jobs', help='Number of jobs', default=-1, type=int)
    args = parser.parse_args()

    logging.debug('Parsed arguments {}'.format(args))
    
    data, labels, groups, lpgo = thutils.read_train_bofs(args.input, args.bofNodes, args.metadata, args.index, norm=args.norm)
    steps = [('clf', classifier())]
    params = {'clf__C': [1, 10, 100]}
    pipeline = sklearn.pipeline.Pipeline(steps)
    logging.debug('Pipeline information {}'.format(pipeline))
    grid_search = sklearn.grid_search.GridSearchCV(pipeline, params, n_jobs=args.jobs, verbose=6, cv=lpgo)
    grid_search.fit(data, labels)
    logging.info('Best score {}'.format(grid_search.best_score_))
    logging.info('Best estimator {}'.format(grid_search.best_estimator_))
    logging.info('Best parameters {}'.format(grid_search.best_estimator_.get_params()))
    logging.info('Scores {}'.format(grid_search.grid_scores_))
