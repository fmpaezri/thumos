import tables as tbl
import numpy

#TODO: make feature lens a function of the window parameters
TR_LEN = 30
HOG_LEN = 96
HOF_LEN = 108
MBHX_LEN = 96
MBHY_LEN = 96

class IDT(tbl.IsDescription):
    frameNum = tbl.UInt32Col(pos=0)
    mean_x = tbl.Float16Col(pos=1)
    mean_y = tbl.Float16Col(pos=2)
    var_x = tbl.Float16Col(pos=3)
    var_y = tbl.Float16Col(pos=4)
    length = tbl.Float16Col(pos=5)
    scale = tbl.Float16Col(pos=6)
    x_pos = tbl.Float16Col(pos=7)
    y_pos = tbl.Float16Col(pos=8)
    t_pos = tbl.Float16Col(pos=9)
    tr = tbl.Float16Col(shape=(TR_LEN,), pos=10)
    hog = tbl.Float16Col(shape=(HOG_LEN,), pos=11)
    hof = tbl.Float16Col(shape=(HOF_LEN,), pos=12)
    mbhx = tbl.Float16Col(shape=(MBHX_LEN,), pos=13)
    mbhy = tbl.Float16Col(shape=(MBHY_LEN,), pos=14)

DTYPE = [('frameNum', numpy.uint32),('mean_x', numpy.float16),('mean_y', numpy.float16),('var_x', numpy.float16),('var_y', numpy.float16),('length', numpy.float16),('scale', numpy.float16),('x_pos', numpy.float16),('y_pos', numpy.float16),('t_pos', numpy.float16),('tr', numpy.float16, (TR_LEN,)),('hog', numpy.float16, (HOG_LEN,)),('hof', numpy.float16, (HOF_LEN,)),('mbhx', numpy.float16, (MBHX_LEN,)),('mbhy', numpy.float16, (MBHY_LEN,))]

def parse(line):
    tokens = line.strip().split()
    index = 0
    frameNum = int(tokens[index])
    index += 1
    mean_x = float(tokens[index])
    index += 1
    mean_y = float(tokens[index])
    index += 1
    var_x = float(tokens[index])
    index += 1
    var_y = float(tokens[index])
    index += 1
    length = float(tokens[index])
    index += 1
    scale = float(tokens[index])
    index += 1
    x_pos = float(tokens[index])
    index += 1
    y_pos = float(tokens[index])
    index += 1
    t_pos = float(tokens[index])
    index += 1
    tr = map(float, tokens[index:index+TR_LEN])
    index += TR_LEN
    hog = map(float, tokens[index:index+HOG_LEN])
    index += HOG_LEN
    hof = map(float, tokens[index:index+HOF_LEN])
    index += HOF_LEN
    mbhx = map(float, tokens[index:index+MBHX_LEN])
    index += MBHX_LEN
    mbhy = map(float, tokens[index:index+MBHY_LEN])
    index += MBHY_LEN
    if not index == len(tokens):
        raise Exception('Descriptor line contains more tokens than expected')
    return [frameNum, mean_x, mean_y, var_x, var_y, length, scale, x_pos, y_pos, t_pos, tr, hog, hof, mbhx, mbhy]

try:
    import cv2
    import numpy
    import tables

    def visualize(featuresPath, nodePath, videoPath):
        '''Generates a visualization of the ITF features from the video and features stored in a node of a H5 feature file'''
        video, node, frames, height, width, delay, featuresWin = init_visualization(featuresPath, nodePath, videoPath)
        overlay_circles(video, node, frames, height, width, delay, featuresWin)

    def init_visualization(featuresPath, nodePath, videoPath, featuresWin='features'):
        #open features file
        itf = tables.open_file(featuresPath, 'r')
        node = itf.get_node(nodePath)

        #open video
        video = cv2.VideoCapture(videoPath)
        if not video.isOpened():
            raise Exception('Video could not be opened')
    
        #get video info
        frames = int(video.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
        width = video.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
        height = video.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
        fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        #try to calculate fps
        if numpy.isnan(fps):
            #skip to end frame
            video.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, int(frames))
            #get position in msec
            msec = video.get(cv2.cv.CV_CAP_PROP_POS_MSEC)
            if numpy.isnan(msec):
                #assume fps
                fps = 30
            else:
                fps = frames/(msec/1000)
        delay = int(1000/fps)
    
        #visualization windows
        cv2.namedWindow(featuresWin)
        return (video, node, frames, height, width, delay, featuresWin)

    def overlay_circles(video, node, frames, height, width, delay, featuresWin, color=cv2.cv.Scalar(0,0,255)):    
        #rewind video if needed, most useful for interactive use
        #commented as some videos only get to FRAMES-1
        #if int(video.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)) == frames:
        video.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0.0)
        for frame in xrange(frames):
            success, image = video.read()
            if success:
                #draw circle on mean position of feature
                [cv2.circle(image, (int(row['y_pos']*height), int(row['x_pos']*width)), 1, color, -1) for row in node.where('(frameNum >= '+str(frame)+')&(frameNum-15 < '+str(frame)+')')]
                cv2.imshow(featuresWin, image)
                cv2.waitKey(delay)
            else:
                raise Exception('Error while reading frame')

except:
    print 'Error importing packages'
