import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

import tables as tbl
import numpy as np
import thutils

features = ['hog_key', 'hof_key', 'tr_key', 'mbh_key']

def bof(inputGroup, output, outputGroup, vocabulary_size=4000):
    samples = inputGroup._v_nchildren
    for key in features:
        logging.debug('Processing key {}'.format(key))
        feature_bof = output.create_earray(outputGroup, key.split('_')[0], atom=tbl.UInt16Atom(), shape=(0, vocabulary_size), createparents=True)
        index = 0
        for sample in inputGroup._f_iter_nodes():
            if not isinstance(sample, tbl.Table):
                logging.debug('Skipping node {}'.format(sample._v_name))
                continue
            current_bof = np.bincount(sample.col(key))
            complete_bof = np.zeros((1,vocabulary_size))
            complete_bof[0,:len(current_bof)] = current_bof
            feature_bof.append(complete_bof)
            index += 1
            if index % (samples / 10) == 0 or index == samples:
                logging.debug('Processing sample {}'.format(index))

def split(node, frames, seconds, fps, vocabulary_size=4000, norm='l2'):
    normalizerStep = thutils.normalizer(norm=norm) if norm in ['l1', 'l2'] else None
    samples = None
    for i in np.arange(0, frames, seconds*fps):
        data = np.array([[row['hog_key'], row['hof_key'], row['tr_key'], row['mbh_key']] for row in node.where('(' + str(i) + ' <= lastFrame) & (lastFrame < ' + str(i+seconds*fps) + ')')])
        full_bof = None
        for f in np.arange(len(features)):
            complete_bof = np.zeros((1,vocabulary_size))
            if len(data) > 0:
                current_bof = np.bincount(data[:,f])
                complete_bof[0,:len(current_bof)] = current_bof
            normalized_bof = normalizerStep.transform(complete_bof) if normalizerStep != None else complete_bof
            if full_bof == None:
                full_bof = normalized_bof
            else:
                full_bof = np.hstack([full_bof, normalized_bof])
        if samples == None:
            samples = full_bof
        else:
            samples = np.vstack([samples, full_bof])
    return samples

def predict_windows(group, metadata, data, clfr, window_length=6):
    nodes = group._f_iter_nodes()
    index = 0
    scores = None
    samples = group._v_nchildren
    names = metadata.col('video_name')
    meta_hash = {}
    for i in np.arange(len(names)):
        meta_hash[names[i]] = i
    for node in nodes:
        if not isinstance(node, tbl.Table):
            logging.debug('Skipping node {}'.format(node._v_name))
            continue
        frames = metadata[meta_hash[node._v_name]]['number_of_frames']
        fps = metadata[meta_hash[node._v_name]]['frame_rate_FPS']
        window_bofs = split(node, frames, window_length, fps)
        Kv = np.dot(window_bofs, data.T)
        probs = clfr.predict_proba(Kv)
        score = np.max(probs, axis=0)
        if scores == None:
            scores = score
        else:
            scores = np.vstack([scores, score])
        index += 1
        if index % ( samples / 10) == 0 or index == samples:
                logging.debug('Processing sample {}'.format(index))
    return scores

import argparse as ap

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Builds a BOF representation of the dataset')
    parser.add_argument('input', help='Input file containing the raw feature')
    parser.add_argument('inputGroup', help='Group holding the tables for each video')
    parser.add_argument('output', help='Output file to hold the BOF representation')
    parser.add_argument('outputGroup', help='Group to hold the generated BOF representation')
    args = parser.parse_args()
    
    input = tbl.open_file(args.input)
    inputGroup = input.get_node(args.inputGroup)
    output = tbl.open_file(args.output, 'a')
    bof(inputGroup, output, args.outputGroup)
    input.close()
    output.close()
