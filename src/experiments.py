import thutils
import tables as tbl
import numpy as np
import sklearn.svm
import scipy.io
import pickle
import sklearn.metrics
import sklearn.multiclass
import sklearn.preprocessing
import bofbuilder

import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

def itf_l2_linear_svm(input='/home/data/thumos/2014/data.h5', bofGroup='/thumos/features/itf/train/bof', metadata='/thumos/metadata/train', output='itf_l2_linear_svm.clf', index='/home/data/thumos/2014/Class Index.txt'):
    h5File = tbl.open_file(input)
    group = h5File.get_node(bofGroup)
    nodes = [node._v_pathname for node in group._f_list_nodes()]
    h5File.close()
    data, labels, groups, lpgo = thutils.read_train_bofs(input, nodes, metadata, index, norm='l2', folds=1)
    K = np.dot(data, data.T)
    clf = sklearn.svm.SVC(kernel='precomputed', probability=True)
    clf.fit(K, labels)
    outputFile = open(output, 'w')
    pickle.dump(clf, outputFile)
    outputFile.close()

def full_l2_itf_linear_svm(input='/home/data/thumos/2014/data_tra_val.h5', bofGroup='/thumos/features/itf/train/bof', metadata='/thumos/metadata/train', index='/home/data/thumos/2014/Class Index.txt', qrels='qrels.txt', tops='tops.txt'):
    h5File = tbl.open_file(input)
    group = h5File.get_node(bofGroup)
    nodes = [node._v_pathname for node in group._f_list_nodes()]
    h5File.close()
    logging.debug('Reading training data')
    data, labels, groups, lpgo = thutils.read_train_bofs(input, nodes, metadata, index, norm='l2', folds=1)
    logging.debug('Label expansion')
    ylabels = sklearn.preprocessing.label_binarize(labels, np.arange(1,thutils.ACTIONS+1))
    logging.debug('Kernel calculation')
    K = np.dot(data, data.T)
    logging.debug('CV')
    scores = []
    for trainIds, testIds in lpgo:
        clf = sklearn.multiclass.OneVsRestClassifier(sklearn.svm.SVC(kernel='precomputed', probability=True))
        logging.debug('Fitting')
        clf.fit(K[trainIds][:, trainIds], ylabels[trainIds])
        logging.debug('Predicting')
        yp = clf.predict_proba(K[testIds][:, trainIds])
        logging.debug('Scoring')
        scores.append(thutils.mAP(yp, ylabels[testIds]))
        locs = np.nonzero(ylabels[testIds])
        queries = locs[1]+1
        docnos = np.array([pair[0] + pair[1]*len(testIds) for pair in np.transpose(locs)])
        rels = np.ones(queries.shape)
        thutils.save_qrels( queries.flat, docnos.flat, rels.flat, qrels)
        queries = (np.ones(ylabels[testIds].shape)*(np.arange(thutils.ACTIONS)+1)).T
        docnos = np.array([pair[1] + pair[0]*len(testIds) for pair in np.transpose(np.nonzero(np.ones(ylabels[testIds].shape).T))])
        sims = yp.T
        run_ids = ['itf_l2_lin_svm']*(len(testIds)*thutils.ACTIONS)
        thutils.save_tops(queries.flat, docnos.flat, sims.flat, run_ids, tops)
    logging.info('Mean score: {}'.format(np.mean(scores)))

def full_l2_itf_linear_svm_single_validation_bof(input='/home/data/thumos/2014/data_tra_val.h5', trainMetadata='/thumos/metadata/train', validationMetadata='/home/data/thumos/2014/validation_set.mat', trainGroup='/thumos/features/itf/train/bof', validationGroup='/thumos/features/itf/validation/bof', index='/home/data/thumos/2014/Class Index.txt'):
    h5File = tbl.open_file(input)
    trainBofGroup = h5File.get_node(trainGroup)
    validationBofGroup = h5File.get_node(validationGroup)
    trainNodes = [node._v_pathname for node in trainBofGroup._f_list_nodes()]
    valNodes = [node._v_pathname for node in validationBofGroup._f_list_nodes()]
    h5File.close()
    logging.debug('Reading features and metadata')
    data, labels, groups, lpgo = thutils.read_train_bofs(input, trainNodes, trainMetadata, index, norm='l2', folds=1)
    logging.debug('Calculating kernel')
    K = np.dot(data, data.T)
    logging.debug('Reading test features')
    h5File = tbl.open_file(input)
    vdata = thutils.read_features(h5File, valNodes, norm='l2')
    ylabels = h5File.root.thumos.metadata.validation.col('actions_indices')
    h5File.close()
    logging.debug('Calculating test kernel')
    Kv = np.dot(vdata, data.T)
    logging.debug('Fitting')
    clf = sklearn.multiclass.OneVsRestClassifier(sklearn.svm.SVC(kernel='precomputed', probability=True))
    clf.fit(K, labels)
    logging.debug('Predicting')
    yp = clf.predict_proba(Kv)
    #ypb = sklearn.preprocessing.label_binarize(yp, np.arange(1,thutils.ACTIONS+1))
    #thutils.classification_submission(zip(names, yp), 'Run1.txt')
    logging.debug('Scoring')
    score = thutils.mAP(yp, ylabels)
    logging.info('Experiment Score: {}'.format(score))

def itf_l2_ovr_linear_svm_val_window_mean():
    trainMetadata='/thumos/metadata/train'
    index='/home/data/thumos/2014/Class Index.txt'
    trainNodes = ['/thumos/features/itf/train/bof/hog', '/thumos/features/itf/train/bof/hof', '/thumos/features/itf/train/bof/tr', '/thumos/features/itf/train/bof/mbh']
    itfPath = '/home/data/thumos/2014/itf.h5'
    itf = tbl.open_file(itfPath)
    validationGroupPath = '/thumos/features/itf/validation'
    validationGroup = itf.get_node(validationGroupPath)
    dataPath = '/home/data/thumos/2014/data.h5'
    data = tbl.open_file(dataPath)
    metadataPath = '/thumos/metadata/validation'
    metadata = data.get_node(metadataPath)
    #logging.debug('Calculating kernel')
    #K = np.dot(data, data.T)
    #logging.debug('Fitting')
    #clf = sklearn.multiclass.OneVsRestClassifier(sklearn.svm.SVC(kernel='precomputed', probability=True))
    #clf.fit(K, labels)
    clfPath = 'src/itf_l2_linear_svm.clf'
    clfFile = open(clfPath)
    clf = pickle.load(clfFile)
    clfFile.close()
    logging.debug('Reading features and metadata')
    data, labels, groups, lpgo = thutils.read_train_bofs(dataPath, trainNodes, trainMetadata, index, norm='l2', folds=1)
    logging.debug('Predicting windows')
    scores = bofbuilder.predict_windows(validationGroup, metadata, data, clf, window_length=6)
    ylabels = metadata.col('actions_indices')
    logging.debug('Scoring')
    score = thutils.mAP(scores, ylabels)
    logging.info('Experiment Score: {}'.format(score))

if __name__ == '__main__':
    #full_l2_itf_linear_svm()
    #full_l2_itf_linear_svm_single_validation_bof()
    itf_l2_ovr_linear_svm_val_window_mean()
