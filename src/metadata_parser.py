import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

import thutils
import tables as tbl
import sklearn.preprocessing
import numpy as np

class FilenameMetadata(tbl.IsDescription):
    action = tbl.StringCol(32)
    group = tbl.UInt8Col()
    clip = tbl.UInt8Col()
    actions_indices = tbl.UInt8Col(shape=(thutils.ACTIONS,))

def generate_train_metadata(inputGroup, output, nodeName):
    #TODO: Create table properly
    tokens = nodeName.split('/')
    where = '/'.join(tokens[:-1])
    name = tokens[-1]
    table = output.create_table(where, name, description=FilenameMetadata, createparents=True)
    row = table.row
    for seq in inputGroup._f_iter_nodes():
        if not isinstance(seq, tbl.Table):
            continue
        metadata = thutils.filename_metadata(seq._v_name)
        row['action'] = metadata[0]
        row['group'] = metadata[1]
        row['clip'] = metadata[2]
        actions = np.zeros((thutils.ACTIONS,), dtype=np.uint8)
        actions[metadata[0]-1] = 1
        row['actions_indices'] = actions
        row.append()
    table.flush()

def generate_partition_metadata(input, output, nodeName):
    metadata = thutils.parse_metadata(input)
    outputh5 = tbl.open_file(output, 'a')
    tokens = nodeName.split('/')
    where = '/'.join(tokens[:-1])
    name = tokens[-1]
    table = outputh5.create_table(where, name, description=thutils.Metadata, createparents=True)
    table.append(metadata)
    table.flush()
    outputh5.close()

import argparse as ap

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Generate the metadata')
    subparsers = parser.add_subparsers(help='Command to generate metadata for different partitions', dest='command')
   
    train = subparsers.add_parser('train', help='Parses metadata from filenames')
    train.add_argument('input', help='Input HDF file with raw data from ITF')
    train.add_argument('inputGroup', help='Group where the nodes have the same name as filename')
    train.add_argument('output', help='Output HDF file with metadata')
    train.add_argument('nodeName', help='Name of node with metadata')

    other = subparsers.add_parser('val_back_test', help='Parses metadata from MAT files')
    other.add_argument('input', help='Input MAT file')
    other.add_argument('output', help='Output HDF file with metadata')
    other.add_argument('nodeName', help='Name of node with metadata')

    args = parser.parse_args()

    if args.command == 'train':
        input = tbl.open_file(args.input, 'a')
        inputGroup = input.get_node(args.inputGroup)
        output = tbl.open_file(args.output, 'a')
        nodeName = args.nodeName
        generate_train_metadata(inputGroup, output, nodeName)
        input.close()
        output.close()
    elif args.command == 'val_back_test':
        generate_partition_metadata(args.input, args.output, args.nodeName)
