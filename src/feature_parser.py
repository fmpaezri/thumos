import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(module)s %(funcName)s %(lineno)d: %(message)s')

import tables as tbl
import argparse as ap

class ITF(tbl.IsDescription):
    lastFrame = tbl.UInt32Col(pos=0)
    mean_x = tbl.Float32Col(pos=1)
    mean_y = tbl.Float32Col(pos=2)
    tr_key = tbl.UInt16Col(pos=3)
    hog_key = tbl.UInt16Col(pos=4)
    hof_key = tbl.UInt16Col(pos=5)
    mbh_key = tbl.UInt16Col(pos=6)

import os
import os.path

def parse_feature_line(values, row):
    index = 0
    row['lastFrame'] = int(values[index])
    index += 1
    row['mean_x'] = float(values[index])
    index += 1
    row['mean_y'] = float(values[index])
    index += 1
    row['tr_key'] = int(values[index])
    index += 1
    row['hog_key'] = int(values[index])
    index += 1
    row['hof_key'] = int(values[index])
    index += 1
    row['mbh_key'] = int(values[index])

def parse_feature_file(args, root, names):
    output = args[0]
    suffix = args[1]
    node = args[2]
    count = 0
    for name in names:
        if not name.endswith(suffix):
            continue
        featureFile = open(os.path.join(root, name))
        table = output.create_table(node, name.split('.')[0], ITF)
        row = table.row
        for line in featureFile:
            tokens = line.strip().split()
            parse_feature_line(tokens, row)
            row.append()
        table.flush()
        count += 1
        if count % 100 == 0:
            logging.info('Parsed {} of {} files on folder {}'.format(count, len(names), root))

def parse_features(root, output, node='/thumos/features/itf', suffix='.txt'):
    if not (os.path.exists(root) and os.path.isdir(root)):
        raise Exception('The path {} doesnt exist or is not a directory'.format(root))
    if os.path.exists(output):
        raise Exception('The output path {} already exists'.format(output))
    output_file = tbl.open_file(output, 'w', filters=tbl.Filters(complevel=9))
    where = '/'.join(node.split('/')[:-1])
    name = node.split('/')[-1]
    output_file.create_group(where, name, createparents=True)
    os.path.walk(root, parse_feature_file, (output_file, suffix, node))

import zipfile

def process_zip_feature_file(zipPath, h5Path, groupNodePath, password=''):
    '''Assumes entries to process are all except directories'''
    h5File = tbl.open_file(h5Path, 'a', filters=tbl.Filters(complevel=9))
    try:
        groupNode = h5File.get_node(groupNodePath)
    except tbl.NoSuchNodeError:
        where = '/'.join(groupNodePath.split('/')[:-1])
        name = groupNodePath.split('/')[-1]
        groupNode = h5File.create_group(where, name, createparents=True)
    logging.info('Opening zip file')
    zipFile = zipfile.ZipFile(zipPath)
    logging.info('Reading zip info')
    zipInfos = [zipInfo for zipInfo in zipFile.infolist() if not zipInfo.filename.endswith('/')]
    itemsToProcess = len(zipInfos)
    
    index = 0
    for zipInfo in zipInfos:
        work(zipFile, zipInfo, h5File, groupNode, password)
        index += 1
        if index == itemsToProcess or index % (itemsToProcess/10) == 0:
            logging.debug('Processed {} items'.format(index))
    logging.info('Finished')

def work(zipFile, zipInfo, h5File, groupNode, password):
    table = create_entry_table(h5File, groupNode, zipInfo)
    fileEntry = zipFile.open(zipInfo.filename, 'r', password)
    process_entry(fileEntry, table)

def create_entry_table(h5File, node, zipInfo):
    tableName = zipInfo.filename.split('/')[-1]
    extensionStart = tableName.index('.')
    tableName = tableName[:extensionStart]
    try:
        table = h5File.create_table(node, tableName, ITF)
    except tbl.NodeError:
        h5File.remove_node(node, tableName)
        table = h5File.create_table(node, tableName, ITF)
    return table

def process_entry(fileEntry, table):
    data = process_plain_feature_file(fileEntry)
    try:
        table.append(data)
    except:
        logging.error('Rows of data for table {}: {}'.format(table._v_name, len(data)))
    table.flush()

def process_plain_feature_file(plainFile):
    lines = plainFile.readlines()
    data = parse_lines(lines)
    return data

def parse_lines(lines):
    data = []
    for line in lines:
        tokens = line.strip().split()
        data.append(parse_tokens(tokens))
    return data
    
def parse_tokens(tokens):
    lastFrame = int(tokens[0])
    meanX = float(tokens[1])
    meanY = float(tokens[2])
    trKey = int(tokens[3])
    hogKey = int(tokens[4])
    hofKey = int(tokens[5])
    mbhKey = int(tokens[6])
    return (lastFrame, meanX, meanY, trKey, hogKey, hofKey, mbhKey)

def parse_dir(args):
    logging.info('Parsing features from dir')
    parse_features(args.path, args.output, args.group, args.suffix)

def parse_zip(args):
    logging.info('Parsing features from zip')
    process_zip_feature_file(args.path, args.output, args.group, args.password)

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='Parse the ITF features as provided by the organizers to a HDF5 table')
    parser.add_argument('path', help='Path to the zip file to parse or folder to walk')
    parser.add_argument('output', help='Path of output HDF5 file')
    parser.add_argument('group', help='Path to the group node to hold the tables of features')
    
    subparsers = parser.add_subparsers(help='Commands to parse from different sources', dest='command')

    dirCommand = subparsers.add_parser('dir', help='Parse the files from a directory structure')
    dirCommand.add_argument('--suffix', help='Suffix to filter file names')
    dirCommand.set_defaults(func=parse_dir)

    zipCommand = subparsers.add_parser('zip', help='Parse the file from a zip file')
    zipCommand.add_argument('--password', help='Password for encrypted files', default='')
    zipCommand.set_defaults(func=parse_zip)

    args = parser.parse_args()
    logging.debug('Args: {}'.format(args))
    args.func(args)
