from IPython.parallel import Client
c = Client(profile='pbs')
import os
import representation
folder = '/ccad0/mindlab/fmpaezri/data/thumos/UCF101'
outputDir = '/ccad0/mindlab/fmpaezri/data/thumos/fvf'
lview = c.load_balanced_view()
sorted_features = sorted([os.path.join(outputDir, filename) for filename in os.listdir(outputDir)])
%time results = lview.map(representation.sample, sorted_features)
import numpy
aSample = numpy.hstack([result for result in results])
len(aSample)
%time results = lview.map(representation.sample, sorted_features, (0.008 for i in xrange(len(sorted_features))))
results.wait_interactive(interval=10.0)
aSample = numpy.hstack([result for result in results])
len(aSample)
cd datasets/thumos/2014/fvf/pca/
%time pcaDict = representation.generate_pca(aSample, dump=True)
cd ../gmm/
%time results = lview.map(representation.sample, sorted_features, (0.002 for i in xrange(len(sorted_features))))
results.wait_interactive(interval=10.0)
aSample = numpy.hstack([result for result in results])
len(aSample)
samples = (aSample[feature] for feature in representation.FEATURE_NAMES)
pcas = (pcaDict[feature] for feature in representation.FEATURE_NAMES)
%time results = lview.map(representation.generate_gmm, samples, pcas)
results.wait_interactive(interval=10.0)
gmmDict = {}
for feature in representation.FEATURE_NAMES:
    gmmDict[feature] = results[representation.FEATURE_NAMES.index(feature)]
%time representation.gmm_params_2_plain('/ccad0/mindlab/fmpaezri/datasets/thumos/2014/fvf/gmm/', gmmDict)
import pickle
for feature in representation.FEATURE_NAMES:
    pickle.dump(gmmDict[feature], open('/ccad0/mindlab/fmpaezri/datasets/thumos/2014/fvf/gmm/'+feature+'_gmm.pkl', 'w'))
fvfDir = '/ccad0/mindlab/fmpaezri/data/thumos/fvf/'
filenames = sorted([os.path.join(fvfDir, filename) for filename in os.listdir(fvfDir)])
inputs = ((filename, pcaDict) for filename in filenames)
dataFolders = ('/ccad0/mindlab/fmpaezri/datasets/thumos/2014/fvf/' for i in xrange(len(filenames)))
%time results = lview.map(representation.fisher_encode, inputs, dataFolders)
results.wait_interactive(interval=10.0)
import tables
newFile  = tables.open_file('/ccad0/mindlab/fmpaezri/datasets/thumos/fvf_fisher.h5', 'w')
table = newFile.create_table('/thumos/fvf/train', 'fisher', description=results[0][1].dtype, createparents=True)
for i in results:
    table.append(i[1])
table.flush()
newFile.close()
