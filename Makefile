dataDir:=/home/data/thumos/2014

trainFeatures:=UCF101_features.zip
trainFeaturesUrl:=http://storage.googleapis.com/crcv/$(trainFeatures)

validationFeatures:=TH14_validation_features.zip
validationFeaturesUrl:=http://storage.googleapis.com/crcv/$(validationFeatures)

backgroundFeatures:=TH14_background_features.zip
backgroundFeaturesUrl:=http://storage.googleapis.com/crcv/$(backgroundFeatures)

testFeatures:=TH14_Test_features.zip
testFeaturesUrl:=http://storage.googleapis.com/crcv/$(testFeatures)

#@PHONY:=$(trainFeatures)

trainVideos:=UCF101_videos.zip
trainVideosUrl:=http://storage.googleapis.com/crcv/$(trainVideos)
validationVideos:=TH14_validation_set_mp4.zip
validationVideosUrl:=http://storage.googleapis.com/crcv/$(validationVideos)
testVideos:=TH14_Test_set_mp4.zip
testVideosUrl:=http://storage.googleapis.com/crcv/$(testVideos)
backgroundVideos:=TH14_background_set_mp4.zip
backgroundVideosUrl:=http://storage.googleapis.com/crcv/$(backgroundVideos)

$(dataDir)/$(trainVideos) downloadTrainVideos:
	wget $(trainVideosUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(validationVideos) downloadValidationVideos:
	wget $(validationVideosUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(testVideos) downloadTestVideos:
	wget $(testVideosUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(backgroundVideos) downloadBackgroundVideos:
	wget $(backgroundVideosUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(trainFeatures) downloadTrainFeatures:
	wget $(trainFeaturesUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(validationFeatures) downloadValidationFeatures:
	wget $(validationFeaturesUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(backgroundFeatures) downloadBackgroundFeatures:
	wget $(backgroundFeaturesUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(testFeatures) downloadTestFeatures:
	wget $(testFeaturesUrl) --directory-prefix=$(dataDir)

itfHdf5Path:=$(dataDir)/itf.h5
password:='THUMOS14_REGISTERED'

zipFeatsToHDF5:
	time python src/feature_parser.py $(dataDir)/$(zipFile) $(itfHdf5Path) $(itfGroup)/$(subGroup) zip --password=$(password)

featsToHDF5:
	make zipFeatsToHDF5 zipFile=$(trainFeatures) subGroup=train password=''
	make zipFeatsToHDF5 zipFile=$(validationFeatures) subGroup=validation password=''
	make zipFeatsToHDF5 zipFile=$(backgroundFeatures) subGroup=background password=''
	make zipFeatsToHDF5 zipFile=$(testFeatures) subGroup=test

itfGroup:=/thumos/features/itf

dataHdf5Path:=$(dataDir)/data.h5

itfBof: $(itfHdf5Path)
	time python src/bofbuilder.py $(itfHdf5Path) $(itfGroup)/$(partition) $(dataHdf5Path) $(itfGroup)/$(partition)/bof

metadataNode:=/thumos/metadata

filenameMetadata:
	time python src/metadata_parser.py train $(itfHdf5Path) $(itfGroup)/$(partition) $(dataHdf5Path) $(metadataNode)/$(partition)

trainFeatureNodes:=$(itfGroup)/mbh $(itfGroup)/hof $(itfGroup)/hog $(itfGroup)/tr

norm:=l2
jobs:=-1

train:
	time python src/train.py $(itfHdf5Path) $(metadataNode) $(trainFeatureNodes) $(dataDir)/$(classIndex) --norm=$(norm) --jobs=$(jobs)

evalkitZip:=THUMOS14_evalkit_20140818.zip
evalkitUrl:=http://crcv.ucf.edu/THUMOS14/eval_kit/$(evalkitZip)

$(dataDir)/$(evalkitZip):
	wget $(evalkitUrl) --directory-prefix=$(dataDir)

evalkitDir:=TH14evalkit

$(dataDir)/$(evalkitDir) unzipEvalkit: | $(dataDir)/$(evalkitZip)
	unzip $| -d $(dataDir)

backgroundMetadataZip:=background_set_meta.zip
backgroundMetadataUrl:=http://crcv.ucf.edu/THUMOS14/Background_set/$(backgroundMetadataZip)

$(dataDir)/$(backgroundMetadataZip):
	wget $(backgroundMetadataUrl) --directory-prefix=$(dataDir)

background_set.mat: | $(dataDir)/$(backgroundMetadataZip)
	unzip -j -P $(password) $| *.mat -d $(dataDir)

validationMetadataZip:=validation_set_meta.zip
validationMetadataUrl:=http://crcv.ucf.edu/THUMOS14/Validation_set/$(validationMetadataZip)

$(dataDir)/$(validationMetadataZip):
	wget $(validationMetadataUrl) --directory-prefix=$(dataDir)

validation_set.mat: | $(dataDir)/$(validationMetadataZip)
	unzip -j -P $(password) $| *.mat -d $(dataDir)

testMetadataMat:=test_set_meta.mat
testMetadataUrl:=http://crcv.ucf.edu/THUMOS14/test_set/$(testMetadataMat)

test_set.mat:
	wget $(testMetadataUrl) -O $(dataDir)/$@

partitionMetadata:
	time python src/metadata_parser.py val_back_test $(dataDir)/$(partition)_set.mat $(dataHdf5Path) $(metadataNode)/$(partition)

temporalAnnotationsValZip:=TH14_Temporal_annotations_validation.zip
temporalAnnotationsValUrl:=http://crcv.ucf.edu/THUMOS14/Validation_set/$(temporalAnnotationsValZip)
temporalAnnotationsValDir:=temp_ann_val

$(dataDir)/$(temporalAnnotationsValZip) downloadTemporalAnnVal:
	wget $(temporalAnnotationsValUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(temporalAnnotationsValDir) unzipTemporalAnnVal: | $(dataDir)/$(temporalAnnotationsValZip)
	unzip -j $| -d $(dataDir)/$(temporalAnnotationsValDir)

temporalAnnotationsTestZip:=TH14_Temporal_annotations_test.zip
temporalAnnotationsTestUrl:=http://crcv.ucf.edu/THUMOS14/test_set/$(temporalAnnotationsTestZip)
temporalAnnotationsTestDir:=temp_ann_test

$(dataDir)/$(temporalAnnotationsTestZip) downloadTemporalAnnTest:
	wget $(temporalAnnotationsTestUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(temporalAnnotationsTestDir) unzipTemporalAnnTest: | $(dataDir)/$(temporalAnnotationsTestZip)
	unzip -j $| -d $(dataDir)/$(temporalAnnotationsTestDir)

classIndex:=Class%20Index.txt
classIndexUrl:=http://crcv.ucf.edu/THUMOS14/$(classIndex)

$(dataDir)/$(classIndex):
	wget $(classIndexUrl) --directory-prefix=$(dataDir)

classIndexDetection:=Class%20Index_Detection.txt
classIndexDetectionUrl:=http://crcv.ucf.edu/THUMOS14/$(classIndexDetection)

$(dataDir)/$(classIndexDetection):
	wget $(classIndexDetectionUrl) --directory-prefix=$(dataDir)

ITFCodebookDir:=THUMOS14_IDTF_Codebook
ITFCodebookZip:=$(ITFCodebookDir).zip
ITFCodebookUrl:=http://crcv.ucf.edu/THUMOS14/$(ITFCodebookZip)

$(dataDir)/$(ITFCodebookZip) downloadCodebook:
	wget $(ITFCodebookUrl) --directory-prefix=$(dataDir)

$(dataDir)/$(ITFCodebookDir) unzipCodebook: | $(dataDir)/$(ITFCodebookZip)
	unzip -P $(password) $| -d $(dataDir)

codebooksGroup:=/thumos/codebooks/itf/kmeans

parseAndSaveCodebook: | $(dataDir)/$(ITFCodebookDir)
	time python src/parse_and_save_codebook.py $| $(dataHdf5Path) $(codebooksGroup)

vlFeatArchive:=vlfeat-0.9.19-bin.tar.gz
vlFeatUrl:=http://www.vlfeat.org/download/$(vlFeatArchive)

/tmp/$(vlFeatArchive) downloadVlFeat:
	wget $(vlFeatUrl) --directory-prefix=/tmp

vlFeatDir:=$(HOME)/utils

unpackVlFeat: | /tmp/$(vlFeatArchive)
	tar xvf $| --directory=$(vlFeatDir)

src/fisher: src/fisher.cpp
	g++ -Wall -g $< -o $@ -L/ccad0/mindlab/fmpaezri/utils/vlfeat-0.9.19/bin/glnxa64 -lvl
